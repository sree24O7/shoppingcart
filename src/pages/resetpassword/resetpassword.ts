import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { HomePage } from '../../pages/home/home';
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html'
})
export class ResetPasswordPage {
  passwordtype: any;
  NewPassword: any;
  ConfirmPassword: any;
  PhoneNumber: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    this.passwordtype = 'password';
    this.PhoneNumber = navParams.get('PhoneNumber');
  }

  showPassword(){
    if (this.passwordtype == 'password'){
      this.passwordtype = 'text';
    } else{
      this.passwordtype = 'password';
    }
  }

  resetPassword(){
    if (this.NewPassword == '' || this.NewPassword == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the New Password',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if (this.ConfirmPassword == '' || this.ConfirmPassword == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the Confirm Password',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if (this.NewPassword != this.ConfirmPassword){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Password Not Matched',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else {
      var data = JSON.stringify({PhoneNumber: this.PhoneNumber, Password: this.ConfirmPassword});
      console.log(data);
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      let options = new RequestOptions({ headers: headers });
       this.http.post(DOMAIN_ADDRESS+'UpdatePassword', data, options)
       .subscribe(data => {
        console.log(data.json().record_count);
        if(data.json().status=='Success'){
          console.log("Oops!");
           let alert = this.alertCtrl.create({
             title: '',
             subTitle: 'Password Updated Successfully',
             buttons: [
                {
                  text: 'OK',
                  handler: (data: any) => {
                    console.log('Yes clicked');
                    this.navCtrl.setRoot(HomePage);
                  }
                }
             ]
           });
           alert.present();
        }
        else{
            let alert = this.alertCtrl.create({
              title: 'Oops!!',
              subTitle: 'Try again please..',
              buttons: ['OK']
            });
            alert.present();
        }
       }, error => {
           console.log("Oops!");
           let alert = this.alertCtrl.create({
             title: 'Oops!!',
             subTitle: 'Something went wrong. Try after sometime',
             buttons: ['OK']
           });
           alert.present();
       });
    }
  }

}
