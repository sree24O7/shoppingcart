import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { ProductPage } from '../../pages/product/product';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  selectedCategory: any;
  icons: string[];
  subCategories: Array<{id: string, name: string, icon: string}>;
  subcategoryShow: boolean = true;
  noSubCategory: boolean = false;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.selectedItem = navParams.get('categoryID');
    this.selectedCategory = navParams.get('categoryName');
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.http.get(DOMAIN_ADDRESS + "GetSubCategoryByCategotry/" + this.selectedItem)
        .subscribe(data => {
            if(data.json().status == 'success') {

              setTimeout(() => {
                loading.dismiss();
              }, 1000);

              if(data.json().record_count === 0) {
                this.noSubCategory = true;
                this.subcategoryShow = false;
              } else {

                this.noSubCategory = false;
                this.subcategoryShow = true;

                  // console.log(data.json().records);
                  var subCategoryList = data.json().records;
                  this.subCategories=[];
                  for(let i = 0; i < subCategoryList.length; i++) {
                    this.subCategories.push({
                      id: subCategoryList[i]['SubCategoryID'],
                      name: subCategoryList[i]['SubCategoryName'],
                      icon: this.icons[i]
                    });
                  }
                  
              }
            } else {
              const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
            }
        }, error => {
            const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
            });
            alert.present();
        });

    // this.items = [];
    // for (let i = 1; i < 11; i++) {
    //   this.items.push({
    //     title: 'Item ' + i,
    //     note: 'This is item #' + i,
    //     icon: this.icons[Math.floor(Math.random() * this.icons.length)]
    //   });
    // }
  }

  itemTapped(event, subCategory) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ProductPage, {
      subCategory: subCategory,
      category: this.selectedItem
    });
  }
}
