import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { API_URL } from '../../app/const';
import { ProductViewPage } from '../../pages/productView/productView';

@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})
export class ProductPage {
  selectedSubcategory : any;
  listView: boolean = true;
  gridView: boolean = false;
  showCartButton: boolean = true;
  showAddedtoCart: boolean = false;
  noProduct: boolean = false;
  productListShow: boolean = true;
  productList: Array<{ categoryID: string, subcategoryID: string, showAddedtoCart: boolean, showCartButton: boolean, productID: string, productName: string, description: string, productCost: string, img: string, img1: string, img2: string, img3: string }>;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams, public loadingCtrl: LoadingController) {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    
     this.selectedSubcategory = navParams.get('subCategory');
     let categoryID = navParams.get('category');
     let subCategoryID = this.selectedSubcategory.id;
     let cartList = [];
     let cartListJSON = localStorage.getItem('myCartList');
     if(cartListJSON === null || cartListJSON === '' || cartListJSON === '[]' || cartListJSON === undefined) {
       
     } else {
        cartList = JSON.parse(cartListJSON);
     }
    //  console.log(cartList.length);
    
    this.http.get(DOMAIN_ADDRESS + "GetProductBasedOnCategory/" + categoryID + "/" + subCategoryID)
        .subscribe(data => {
            if(data.json().status == 'success') {
              setTimeout(() => {
                loading.dismiss();
              }, 2000);
              if(data.json().record_count === 0) {
                console.log(data.json());
                this.productListShow = false;
                this.noProduct = true;
              } else {

                this.noProduct = false;
                this.productListShow = true;
                
                var products = data.json().records;
                this.productList=[];
                let showCart = true;
                let showAdded = false;
                for(let i = 0; i < products.length; i++) {
                  if(cartList.length > 0) {
                    for( let j = 0; j < cartList.length; j++) {
                      if(cartList[j]['productID'] ===  products[i]['product_id']) {
                        showCart = false;
                        showAdded = true;
                      }
                    }
                  }
                  this.productList.push({
                    categoryID: products[i]['category_id'],
                    subcategoryID: products[i]['sub_category_id'],
                    productID: products[i]['product_id'],
                    productName: products[i]['product_name'],
                    description: products[i]['product_desc'],
                    productCost: products[i]['productCost'],
                    showCartButton: showCart,
                    showAddedtoCart: showAdded,
                    img: `${API_URL}${products[i]['productImg']}`,
                    img1: `${API_URL}${products[i]['productImg1']}`,
                    img2: `${API_URL}${products[i]['productImg2']}`,
                    img3: `${API_URL}${products[i]['productImg3']}`
                  });
                }
              } 
            } else {
              const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();

              loading.dismiss();
            }
        }, error => {
            const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
            });
            alert.present();

            loading.dismiss();
        });
  }
  changeView() {
      this.listView = !this.listView;
      this.gridView = !this.gridView;
  }
  productSelected(product) {
    this.navCtrl.push(ProductViewPage, {
      product: product,
    });
  }
  // addToWishList(product) {
  //   console.log(JSON.stringify(product));
  // }
  addToCart(product, index) {
    let productArray = [];
    let cartListJSON = localStorage.getItem('myCartList');
    if(cartListJSON === null || cartListJSON === '' || cartListJSON === '[]' || cartListJSON === undefined) {
      productArray.push(product);
      localStorage.setItem('myCartList', JSON.stringify(productArray));
      // console.log(localStorage.getItem('myCartList'));
    } else {
      productArray = JSON.parse(cartListJSON);
      productArray.push(product);
      localStorage.setItem('myCartList', JSON.stringify(productArray));
      // console.log(localStorage.getItem('myCartList'));
      this.productList[index]['showCartButton']= false;
      this.productList[index]['showAddedtoCart']= true;
    }
  }
}
