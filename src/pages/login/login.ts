import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { SignupPage } from '../../pages/signup/signup';
import { ForgotPasswordPage } from '../../pages/forgotpassword/forgotpassword';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  passwordtype: any;
  PhoneNumber: any;
  Password: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
      this.passwordtype = 'password';
  }

  showPassword(){
    if (this.passwordtype == 'password'){
      this.passwordtype = 'text';
    } else{
      this.passwordtype = 'password';
    }
  }

  signup(){
    this.navCtrl.push(SignupPage);
  }

  forgotpassword(){
    this.navCtrl.push(ForgotPasswordPage);
  }

  login(){
    if (this.PhoneNumber == '' || this.PhoneNumber == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the Phone Number',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(isNaN(this.PhoneNumber)){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Phone Number cannot be in characters',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(this.PhoneNumber.length != 10){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Phone Number length must be 10',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(this.Password == '' || this.Password == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the Password',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else {
      var data = JSON.stringify({PhoneNumber: this.PhoneNumber,Password:this.Password});
      console.log(data);
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      let options = new RequestOptions({ headers: headers });
       this.http.post(DOMAIN_ADDRESS+'LoginUserApp', data, options)
       .subscribe(data => {
        console.log(data.json().record_count);
        if(data.json().record_count==0){
          console.log("Oops!");
           let alert = this.alertCtrl.create({
             title: 'Oops!!',
             subTitle: 'Please check your Phone Number or Password',
             buttons: ['OK']
           });
           alert.present();
        }
        else{
          console.log(data.json().records);
          localStorage.setItem('ShoppingCartDetails',JSON.stringify(data.json().records));
          this.PhoneNumber="";
          this.Password="";
             this.navCtrl.setRoot(HomePage);
        }
       }, error => {
           console.log("Oops!");
           let alert = this.alertCtrl.create({
             title: 'Oops!!',
             subTitle: 'Something went wrong. Try after sometime',
             buttons: ['OK']
           });
           alert.present();
       });

    }
  }

  goback(){
    this.navCtrl.setRoot(HomePage);
  }
}
