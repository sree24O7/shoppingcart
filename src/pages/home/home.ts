import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { DOMAIN_ADDRESS } from '../../app/const';
import { API_URL } from '../../app/const';
import { Http } from '@angular/http';
import { ProductViewPage } from '../../pages/productView/productView';
import { cartPage } from '../../pages/cart/cart';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeImage : Array<{ image1: string, image2: string, image3: string, image4: string, image5: string }>;
  img1: any;
  img2: any;
  img3: any;
  img4: any;
  img5: any;
  productList: Array<{ categoryID: string, subcategoryID: string, productID: string, productName: string, description: string, productCost: string, img: string, img1: string, img2: string, img3: string }>;

  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    // this.homeImage.push({
    //   image1: `${API_URL}uploads/HomeImage/homeImage1.png`,
    //   image2: `${API_URL}uploads/HomeImage/homeImage2.png`,
    //   image3: `${API_URL}uploads/HomeImage/homeImage3.png`,
    //   image4: `${API_URL}uploads/HomeImage/homeImage4.png`,
    //   image5: `${API_URL}uploads/HomeImage/homeImage5.png`
    // });
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.img1 = `${API_URL}HomeImages/homeImage1.png`;
    this.img2 = `${API_URL}HomeImages/homeImage2.png`;
    this.img3 = `${API_URL}HomeImages/homeImage3.png`;
    this.img4 = `${API_URL}HomeImages/homeImage4.png`;
    this.img5 = `${API_URL}HomeImages/homeImage5.png`;
    this.http.get(DOMAIN_ADDRESS + "GetRandomProduct")
        .subscribe(data => {
            if(data.json().status == 'success') {
              setTimeout(() => {
                loading.dismiss();
              }, 2000);
              if(data.json().record_count === 0) {
                console.log(data.json());
              } else {
                var products = data.json().records;
                this.productList=[];
                for(let i = 0; i < products.length; i++) {
                  this.productList.push({
                    categoryID: products[i]['category_id'],
                    subcategoryID: products[i]['product_id'],
                    productID: products[i]['product_id'],
                    productName: products[i]['product_name'],
                    description: products[i]['product_desc'],
                    productCost: products[i]['productCost'],
                    img: `${API_URL}${products[i]['productImg']}`,
                    img1: `${API_URL}${products[i]['productImg1']}`,
                    img2: `${API_URL}${products[i]['productImg2']}`,
                    img3: `${API_URL}${products[i]['productImg3']}`
                  });
                }
              } 
            } else {
              const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();

              loading.dismiss();
            }
        }, error => {
            const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
            });
            alert.present();

            loading.dismiss();
        });

  }
  gotoProductView(product) {
    this.navCtrl.push(ProductViewPage, {
      product: product,
    });
  }
  MyCart() {
    this.navCtrl.push(cartPage);
  }
}
