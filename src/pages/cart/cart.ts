import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, RequestOptions, Headers  } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { API_URL } from '../../app/const';
import { orderPage } from '../../pages/order/order';
import { HomePage } from '../../pages/home/home';


@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html'
})
export class cartPage {
  myCartList: Array<{ categoryID: string, subcategoryID: string, productID: string, productName: string, description: string, productCost: string, finalAmount: string, size: string, img: string, img1: string, img2: string, img3: string }>;
  emptyCart: boolean = false;
  showCartList: boolean = true;
  totalAmount: number;
  showAddressBlock: boolean = false;
  addressLine1: any;
  addressLine2: any;
  addressLine3: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    let cartListJSON = localStorage.getItem('myCartList');
    // console.log(cartListJSON);
    if(cartListJSON === null || cartListJSON === '[]' || cartListJSON === '' || cartListJSON === undefined) {
      this.emptyCart = true;
      this.showCartList = false;
    } else {
      let products = JSON.parse(cartListJSON);
      let cartList = [];
      let amount = 0;
      for(let i = 0; i < products.length; i++) {
        amount += parseInt(products[i]['productCost']);
        this.totalAmount = amount;
          cartList.push({
            categoryID: products[i]['categoryID'],
            subcategoryID: products[i]['subcategoryID'],
            productID: products[i]['productID'],
            productName: products[i]['productName'],
            description: products[i]['description'],
            productCost: products[i]['productCost'],
            finalAmount: products[i]['productCost'],
            size: products[i]['size'],
            quantity: 1, //products[i]['quantity'],
            img: products[i]['img'],
            img1: products[i]['img1'],
            img2: products[i]['img2'],
            img3:  products[i]['img3']
          });
        }
        this.myCartList = cartList;
        // console.log(JSON.stringify(this.myCartList));
    }
  }
  increaseQuantity(i) {
    let cost = parseInt(this.myCartList[i]['productCost']);
    let quantity = this.myCartList[i]['quantity'] + 1;
    this.myCartList[i]['quantity'] = quantity;
    let totalCost = cost * quantity;
    this.myCartList[i]['finalAmount'] = totalCost.toString();
    localStorage.setItem('myCartList', JSON.stringify(this.myCartList));

    this.totalAmount = this.totalAmount + cost;
  }
  decreaseQuantity(i) {
    if(this.myCartList[i]['quantity'] > 0) {
      let cost = parseInt(this.myCartList[i]['productCost']);
      let quantity = this.myCartList[i]['quantity'] - 1;
      this.myCartList[i]['quantity'] = quantity;
      let totalCost = cost * quantity;
      this.myCartList[i]['finalAmount'] = totalCost.toString();
      localStorage.setItem('myCartList', JSON.stringify(this.myCartList));

      this.totalAmount = this.totalAmount - cost;
    }
  }
  deleteFromCart(index) {
      let alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Are you sure ?',
      buttons: [
              {
                text: 'Yes',
                handler: (data: any) => {
                  this.totalAmount = this.totalAmount - parseInt(this.myCartList[index]['finalAmount']);
                  this.myCartList.splice(index, 1);
                  // console.log(JSON.stringify(this.myCartList[index]));
                  localStorage.setItem('myCartList', JSON.stringify(this.myCartList));
                  if(this.myCartList.length == 0) {
                    this.emptyCart = true;
                    this.showCartList = false;
                  } 
                }
              },
              { 
                text: 'No',
                role: 'cancel',
                handler: () => {
                  // console.log('Cancel clicked');
                }
              }
          ]
    });
    alert.present();
  }
  checkoutFunction() {
    
    // let orderInsertJSON = {UserName: userName, UserID: userID, PhoneNumber: phoneNumber, ProductID: productID, Address: address, Quantity: this.quantity, size: this.size};
      // console.log(JSON.stringify(orderInsertJSON));
      if(this.showAddressBlock) {
          this.addAddress();
      } else {
        this.showCartList = false;
        this.emptyCart = false;
        this.showAddressBlock = true;
      }
  }
  addAddress() {
    // console.log(this.addressLine1+" , "+this.addressLine2+" , "+this.addressLine3);
    // localStorage.getItem("myAddressList");
    // console.log("AddAddress");
    let checkoutDetails = localStorage.getItem('ShoppingCartDetails');
    let userName = '';
    let userID = '';
    let phoneNumber = '';
    let address = '';
    let productID = '';
    let addressArray = [];
    if(this.addressLine1 !== undefined && this.addressLine2 !== undefined && this.addressLine3 !== undefined) {
      addressArray.push({
        addressLine1: this.addressLine1,
        addressLine2: this.addressLine2,
        addressLine3: this.addressLine3
      });
      localStorage.setItem("myAddressList", JSON.stringify(addressArray));
      address = `${this.addressLine1},${this.addressLine2},${this.addressLine3}`;
    } else {
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Please enter the address details',
        buttons: ['OK']
      });
      alert.present();
    }
    if(checkoutDetails != '' || checkoutDetails != undefined || checkoutDetails != null) {
      checkoutDetails = JSON.parse(checkoutDetails);
      if(checkoutDetails != null) {
        userName = `${checkoutDetails[0]['FirstName']}${checkoutDetails[0]['LastName']}`
        userID = checkoutDetails[0]['UserID'];
        phoneNumber = checkoutDetails[0]['phoneNumber'];
      }
    }

    let insertArray = [];
    for(let i = 0; i < this.myCartList.length; i++) {
      insertArray.push({
        UserName: userName,
        UserID: userID,
        PhoneNumber: phoneNumber,
        Address: address,
        ProductID: this.myCartList[i]['productID'],
        Quantity: this.myCartList[i]['quantity'],
        size: this.myCartList[i]['size']
      });
    }
    console.log(JSON.stringify(insertArray));
      // let orderInsertJSON = {UserName: userName, UserID: userID, PhoneNumber: phoneNumber, ProductID: productID, Address: address, Quantity: this.quantity, size: this.size};
      // console.log(JSON.stringify(orderInsertJSON));


    var headers = new Headers();
          headers.append("Accept", 'application/json');
          headers.append('Content-Type', 'application/json' );
          let options = new RequestOptions({ headers: headers });
          this.http.post(DOMAIN_ADDRESS+'CheckoutOrderInsert', JSON.stringify(insertArray), options)
          .subscribe(data => {
            console.log(data.json().record_count);
            if(data.json().status == 'Success'){
              localStorage.setItem('myCartList', '');
              let alert = this.alertCtrl.create({
                title: 'Success!!',
                subTitle: 'Ordered Successfully',
                buttons: ['OK']
              });
              alert.present();
              this.navCtrl.setRoot(HomePage);
            }
            else{
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
            }
          }, error => {
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
          });
  }
}
