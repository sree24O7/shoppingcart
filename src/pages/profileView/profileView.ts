import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { ProfilePage } from '../../pages/profile/profile';
import { HomePage } from '../../pages/home/home';


@Component({
  selector: 'page-profileView',
  templateUrl: 'profileView.html'
})
export class ProfileViewPage {
  UserID: any;
  UserType: any;
  FirstName: any;
  LastName: any;
  PhoneNumber: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
      let userData = JSON.parse(localStorage.getItem('ShoppingCartDetails'));
      this.FirstName = userData[0].FirstName;
      this.LastName =  userData[0].LastName;
      this.PhoneNumber =  userData[0].phoneNumber;
      this.UserType = userData[0].Gender;

  }
  gotoProfileEdit() {
    localStorage.setItem('isUpdateUser', 'true');
    this.navCtrl.push(ProfilePage);
  }
  LogOut() {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Do you want to Log Out ?',
      buttons: [
              {
                text: 'Yes',
                handler: (data: any) => {
                  localStorage.setItem('ShoppingCartDetails', '');
                  this.navCtrl.setRoot(HomePage);
                }
              },
              { 
                text: 'No',
                role: 'cancel',
                handler: () => {
                  // console.log('Cancel clicked');
                }
              }
          ]
    });
    alert.present();

  }
}
