import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { OTPPage } from '../../pages/otp/otp';
import { LoginPage } from '../../pages/login/login';
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html'
})
export class ForgotPasswordPage {
  PhoneNumber: any;
  Passcode: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    
  }

  generateOTP(){
      if (this.PhoneNumber == '' || this.PhoneNumber == undefined){
        let alert = this.alertCtrl.create({
          title: 'Oops!!',
          subTitle: 'Enter the PhoneNumber',
          buttons: ['OK']
        });
        alert.present();
        return false;
      } else {
        var data = JSON.stringify({PhoneNumber: this.PhoneNumber});
        console.log(data);
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
         this.http.post(DOMAIN_ADDRESS+'CheckUserExist', data, options)
         .subscribe(data => {
          console.log(data.json().record_count);
          if(data.json().status=='Success'){
            if (data.json().record_count != 0){
              var headers = new Headers();
              headers.append("Accept", 'application/json');
              headers.append('Content-Type', 'application/json' );
              let options = new RequestOptions({ headers: headers });
               this.http.post(DOMAIN_ADDRESS+'GenerateOTP', data, options)
               .subscribe(data => {
                console.log(data.json().record_count);
                if(data.json().status=='Success'){
                  this.Passcode = data.json().passcode;
                  console.log("Oops!");
                   let alert = this.alertCtrl.create({
                     title: '',
                     subTitle: 'OTP sent to your Mobile',
                     buttons: [
                        {
                          text: 'OK',
                          handler: (data: any) => {
                            console.log('Yes clicked');
                            this.navCtrl.push(OTPPage, {
                              PhoneNumber: this.PhoneNumber,
                              Passcode: this.Passcode,
                              OTPFrom: 'ForgotPassword'
                            });
                          }
                        }
                     ]
                   });
                   alert.present();
                }
                else{
                    let alert = this.alertCtrl.create({
                      title: 'Oops!!',
                      subTitle: 'Try again please..',
                      buttons: ['OK']
                    });
                    alert.present();
                }
               }, error => {
                   console.log("Oops!");
                   let alert = this.alertCtrl.create({
                     title: 'Oops!!',
                     subTitle: 'Something went wrong. Try after sometime',
                     buttons: ['OK']
                   });
                   alert.present();
               });
            } else {
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Phone Number not exist',
                buttons: ['OK']
              });
              alert.present();
           }
          }
         }, error => {
             console.log("Oops!");
             let alert = this.alertCtrl.create({
               title: 'Oops!!',
               subTitle: 'Something went wrong. Try after sometime',
               buttons: ['OK']
             });
             alert.present();
         });


        
      }
    }
    
    goback(){
      this.navCtrl.push(LoginPage);
    }
}
