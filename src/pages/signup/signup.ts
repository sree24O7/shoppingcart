import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { LoginPage } from '../../pages/login/login';
import { ProfilePage } from '../../pages/profile/profile';
import { OTPPage } from '../../pages/otp/otp';
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  passwordtype: any;
  PhoneNumber: any;
  Password: any;
  Passcode: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
      var localData = JSON.parse(localStorage.getItem("patientInventoryDetails"));
      this.passwordtype = 'password';
  }

  signin(){
    this.navCtrl.push(LoginPage);
  }

  signup(){
    if (this.PhoneNumber == '' || this.PhoneNumber == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the PhoneNumber',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(isNaN(this.PhoneNumber)){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Phone Number cannot be in characters',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(this.PhoneNumber.length != 10){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Phone Number length must be 10',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(this.Password == '' || this.Password == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter the Password',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if(this.Password.length < 6){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Password length cannot be less than 6',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else {
      var data = JSON.stringify({PhoneNumber: this.PhoneNumber});
      console.log(data);
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      let options = new RequestOptions({ headers: headers });
      this.http.post(DOMAIN_ADDRESS+'CheckUserExist', data, options)
      .subscribe(data => {
       console.log(data.json().record_count);
       if(data.json().status=='Success'){
         if (data.json().record_count == 0){
            
            this.navCtrl.setRoot(ProfilePage, {PhoneNumber: this.PhoneNumber,Password: this.Password});
            

            // this.http.post(DOMAIN_ADDRESS+'GenerateOTP', data, options)
            // .subscribe(data => {
            //   console.log(data.json().record_count);
            //   if(data.json().status=='Success'){
            //     this.Passcode = data.json().passcode;
            //     console.log("Oops!");
            //     let alert = this.alertCtrl.create({
            //       title: '',
            //       subTitle: 'OTP sent to your Mobile',
            //       buttons: [
            //               {
            //                 text: 'OK',
            //                 handler: (data: any) => {
            //                   console.log('Yes clicked');
            //                   this.navCtrl.push(OTPPage, {
            //                     PhoneNumber: this.PhoneNumber,
            //                     Password: this.Password,
            //                     Passcode: this.Passcode,
            //                     OTPFrom: 'SignUp'
            //                   });
            //                 }
            //               }
            //           ]
            //     });
            //     alert.present();
            //   }
            //   else{
            //       let alert = this.alertCtrl.create({
            //         title: 'Oops!!',
            //         subTitle: 'Try again please..',
            //         buttons: ['OK']
            //       });
            //       alert.present();
            //   }
            // }, error => {
            //     console.log("Oops!");
            //     let alert = this.alertCtrl.create({
            //       title: 'Oops!!',
            //       subTitle: 'Something went wrong. Try after sometime',
            //       buttons: ['OK']
            //     });
            //     alert.present();
            // });
         } else {
            let alert = this.alertCtrl.create({
              title: 'Oops!!',
              subTitle: 'Phone Number already exist',
              buttons: ['OK']
            });
            alert.present();
         }
       }
      }, error => {
          console.log("Oops!");
          let alert = this.alertCtrl.create({
            title: 'Oops!!',
            subTitle: 'Something went wrong. Try after sometime',
            buttons: ['OK']
          });
          alert.present();
      });



     
    }
  }
}
