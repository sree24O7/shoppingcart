import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions  } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { HomePage } from '../../pages/home/home';
import { Content } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  UserID: any;
  UserType: any;
  FirstName: any;
  LastName: any;
  PhoneNumber: any;
  Password: any;
  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    this.UserID = navParams.get('UserID');
    this.Password = navParams.get('Password');
    this.PhoneNumber = navParams.get('PhoneNumber');
    let userData = localStorage.getItem('ShoppingCartDetails');
    if(userData !== '' && userData !== null && userData !== undefined) {
      this.UserID = JSON.parse(userData)[0].UserID;
    }
  }

  UserTypeChange (usertype){
    this.UserType = usertype
  }

  public scrollElement() {
    debugger;
    let element = document.getElementById('target');
    this.content.scrollTo(0, element.offsetTop, 500);
  }

  UpdateProfile(){
    if (this.UserType == '' || this.UserType == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Select UserType',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if (this.FirstName == '' || this.FirstName == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter FirstName',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else if (this.LastName == '' || this.LastName == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Enter LastName',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else {
      console.log('test');


      var isUpdate = localStorage.getItem('isUpdateUser');
      if (isUpdate === 'true') {
          localStorage.setItem('isUpdateUser','');
          var dataSignup = JSON.stringify({UserType: this.UserType,FirstName: this.FirstName, LastName: this.LastName, UserID: this.UserID});
          console.log(dataSignup);
          var headers = new Headers();
          headers.append("Accept", 'application/json');
          headers.append('Content-Type', 'application/json' );
          let options = new RequestOptions({ headers: headers });
          this.http.post(DOMAIN_ADDRESS+'UpdateUserApp', dataSignup, options)
          .subscribe(data => {
            console.log(data.json().record_count);
            if(data.json().status=='Success'){
              console.log(data.json().records+"<== at update user");
              localStorage.setItem('ShoppingCartDetails',JSON.stringify(data.json().records));
              this.navCtrl.setRoot(HomePage);
            }
            else{
                let alert = this.alertCtrl.create({
                  title: 'Oops!!',
                  subTitle: 'Failed to update',
                  buttons: ['OK']
                });
                alert.present();
            }
          }, error => {
              console.log("Oops!");
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons:['OK']
              });
              alert.present();
          });
      } else {
        var userData = JSON.stringify({PhoneNumber: this.PhoneNumber,Password: this.Password});
        console.log(userData);
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });
          this.http.post(DOMAIN_ADDRESS+'CreateUserApp', userData, options)
          .subscribe(data => {
          
          if(data.json().status=='Success'){
            // this.navCtrl.setRoot(ProfilePage, {UserID: data.json().UserID});
              var dataSignup = JSON.stringify({UserType: this.UserType,FirstName: this.FirstName, LastName: this.LastName, UserID: data.json().UserID});
              console.log(dataSignup);
              var headers = new Headers();
              headers.append("Accept", 'application/json');
              headers.append('Content-Type', 'application/json' );
              let options = new RequestOptions({ headers: headers });
              this.http.post(DOMAIN_ADDRESS+'UpdateUserApp', dataSignup, options)
              .subscribe(data => {
                console.log(data.json().record_count);
                if(data.json().status=='Success'){
                  console.log(data.json().records+"<== at create user");
                  localStorage.setItem('ShoppingCartDetails',JSON.stringify(data.json().records));
                  this.navCtrl.setRoot(HomePage);
                }
                else{
                    let alert = this.alertCtrl.create({
                      title: 'Oops!!',
                      subTitle: 'Failed to update',
                      buttons: ['OK']
                    });
                    alert.present();
                }
              }, error => {
                  console.log("Oops!");
                  let alert = this.alertCtrl.create({
                    title: 'Oops!!',
                    subTitle: 'Something went wrong. Try after sometime',
                    buttons:['OK']
                  });
                  alert.present();
              });
          }
          else{
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Try again please..',
                buttons: ['OK']
              });
              alert.present();
          }
          }, error => {
              console.log("Oops!");
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
              });
              alert.present();
          });
      }
    }
  }
}
