import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController, ModalController, Platform } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { API_URL } from '../../app/const';
import { HomePage } from '../../pages/home/home';


@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})
export class orderPage {
  product: any;
  size: any;
  sizeText: any;
  quantity: any;
  amount: any;
  address: any;
  showAddressBlock: boolean = false;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    this.product = navParams.get('product');
    this.size = navParams.get('size');
    this.quantity = 1;
    this.amount = this.product.productCost;

    let addressData = localStorage.getItem("myAddressList");
    console.log(addressData+"  <=== address data");
    if(addressData != null && addressData != undefined && addressData != '') {
      
      if(addressData != 'null') {
        console.log("comes here");
        addressData = JSON.parse(addressData);
        this.address = addressData;
        this.showAddressBlock = true;
      }
    }
    
    // if(this.size == "S") {
    //     this.sizeText = 'Small';
    // } else if(this.size == "M") {
    //     this.sizeText = 'Medium';
    // } else if(this.size == "L") {
    //     this.sizeText = 'Large';
    // } else if(this.size == "XL") {
    //     this.sizeText = 'Extra-Large(XL)';
    // } else {
    //     this.sizeText = 'XXL';
    // }
    // console.log(JSON.stringify(this.product));
  }
  increaseCount() {
    // var quantity = parseInt(this.quantity);
    // console.log(quantity);
    if(this.quantity > 0) {
      this.amount = this.product.productCost;
      this.quantity = this.quantity + 1;
      this.amount = this.amount * this.quantity;
    }
  }
  decreaseCount() {
    if(this.quantity > 0 && this.quantity != 1) {
      this.amount = this.product.productCost;
      this.quantity = this.quantity - 1;
      this.amount = this.amount * this.quantity;
    }
  }
  placeOrder() {
    let checkoutDetails = localStorage.getItem('ShoppingCartDetails');
    let userName = '';
    let userID = '';
    let phoneNumber = '';
    let address = '';
    let productID = '';
    let checkoutArr = [];

    if(checkoutDetails != '' && checkoutDetails != undefined && checkoutDetails != 'undefined') {
      checkoutArr = JSON.parse(checkoutDetails);
      userName = `${checkoutArr[0]['FirstName']}${checkoutArr[0]['LastName']}`
      userID = checkoutArr[0]['UserID'];
      phoneNumber = checkoutArr[0]['phoneNumber'];
    }
    if(this.showAddressBlock) {
      address = `${this.address[0].addressLine1},${this.address[0].addressLine2},${this.address[0].addressLine3}`;
      productID = this.product.productID;
      // console.log(address+" , "+productID+" , "+userName+" , "+userID+" , "+phoneNumber);
      let orderInsertJSON = {UserName: userName, UserID: userID, PhoneNumber: phoneNumber, ProductID: productID, Address: address, Quantity: this.quantity, size: this.size};
      console.log(JSON.stringify(orderInsertJSON));
          var headers = new Headers();
          headers.append("Accept", 'application/json');
          headers.append('Content-Type', 'application/json' );
          let options = new RequestOptions({ headers: headers });
          this.http.post(DOMAIN_ADDRESS+'InsertOrder', orderInsertJSON, options)
          .subscribe(data => {
            console.log(data.json().record_count);
            if(data.json().status == 'Success'){
              let alert = this.alertCtrl.create({
                title: 'Success!!',
                subTitle: 'Ordered Successfully',
                buttons: ['OK']
              });
              alert.present();
              this.navCtrl.setRoot(HomePage);
            }
            else{
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
            }
          }, error => {
              let alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
          });



    } else {
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Please add an address',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  openModal(param) {
    let modal = this.modalCtrl.create(ModalContentPage, param);
     modal.onDidDismiss(data => {
      // console.log(data+"<= Address Data");
      if(data !== undefined && data !== null && data !== '') {
        this.address = data;
        this.showAddressBlock = true;
      }
     });
    modal.present();
  }
}

@Component({
  template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Address
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary" showWhen="ios">Cancel</span>
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content>
  <ion-list>
      
  </ion-list>
  <ion-grid *ngIf="showAddAddress">
    <ion-row>
      <ion-item>
        <ion-label floating>Address Line 1</ion-label>
        <ion-input type="text" [(ngModel)]="addressLine1"></ion-input>
      </ion-item>
      <ion-item>
        <ion-label floating>Address Line 2</ion-label>
        <ion-input type="text" [(ngModel)]="addressLine2"></ion-input>
      </ion-item>
      <ion-item>
        <ion-label floating>Address Line 3</ion-label>
        <ion-input type="text" [(ngModel)]="addressLine3"></ion-input>
      </ion-item>
      <ion-col col-12 align="center" style="margin-top: 3%">
        <button ion-button (click)="addAddress()" full style="background-color: #AD1457 !important; color: #FFFFFF;">Submit</button>
      </ion-col>
    </ion-row>
  </ion-grid>
</ion-content>
`
})
export class ModalContentPage {
  addressList: Array<{}>;
  showAddAddress: boolean = true;
  addressLine1: any;
  addressLine2: any;
  addressLine3: any;
  
  constructor(public platform: Platform, public navCtrl: NavController, public params: NavParams, public viewCtrl: ViewController,  public alertCtrl: AlertController) {
    let addresses=[];
    // let addressJSON = localStorage.getItem("myAddressList");
    // if(addressJSON === null || addressJSON === undefined || addressJSON === '') {
    //   this.showAddAddress = !this.showAddAddress;
    // }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  addAddress() {
    // console.log(this.addressLine1+" , "+this.addressLine2+" , "+this.addressLine3);
    // localStorage.getItem("myAddressList");
    let addressArray = [];
    if(this.addressLine1 !== undefined && this.addressLine2 !== undefined && this.addressLine3 !== undefined) {
      addressArray.push({
        addressLine1: this.addressLine1,
        addressLine2: this.addressLine2,
        addressLine3: this.addressLine3
      });
      localStorage.setItem("myAddressList", JSON.stringify(addressArray));
      this.viewCtrl.dismiss(addressArray);
      // this.navCtrl.setRoot();
    } else {
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Please enter the address details',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
