import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions  } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { LoginPage } from '../../pages/login/login';
import { SignupPage } from '../../pages/signup/signup';
import { ProfilePage } from '../../pages/profile/profile';
import { ResetPasswordPage } from '../../pages/resetpassword/resetpassword';

@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html'
})
export class OTPPage {
  PhoneNumber: any;
  Password: any;
  Passcode: any;
  PageFrom: any;
  OTP1: any
  OTP2: any;
  OTP3: any;
  OTP4: any;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
     this.PhoneNumber = navParams.get('PhoneNumber');
     this.Password = navParams.get('Password');
     this.Passcode = navParams.get('Passcode');
     this.PageFrom = navParams.get('OTPFrom');
  }

  signin(){
    this.navCtrl.push(LoginPage);
  }

  generateOTP(){
    var data = JSON.stringify({PhoneNumber: this.PhoneNumber});
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
     this.http.post(DOMAIN_ADDRESS+'GenerateOTP', data, options)
     .subscribe(data => {
      console.log(data.json().record_count);
        if(data.json().status=='Success'){
          this.Passcode = data.json().passcode;
          console.log("Oops!");
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: 'OTP sent to your Mobile',
            buttons: ['OK']
          });
          alert.present();
        }
      }, error => {
        console.log("Oops!");
        let alert = this.alertCtrl.create({
          title: 'Oops!!',
          subTitle: 'Something went wrong. Try after sometime',
          buttons: [
          {
            text: 'OK',
            handler: (data: any) => {
              console.log('Yes clicked');
              this.navCtrl.push(SignupPage);
            }
          }
      ]
        });
        alert.present();
    });
  }

  next(){
    var otp = this.OTP1 + this.OTP2 + this.OTP3 + this.OTP4;
    console.log(otp);
    debugger;
    if (this.OTP1 == '' || this.OTP1 == undefined || this.OTP2 == '' || this.OTP2 == undefined || this.OTP3 == '' || this.OTP3 == undefined || this.OTP4 == '' || this.OTP4 == undefined){
      let alert = this.alertCtrl.create({
        title: 'Oops!!',
        subTitle: 'Please Enter OTP',
        buttons: ['OK']
      });
      alert.present();
      return false;
    } else {
      if (this.PageFrom == 'SignUp'){
          if (otp == this.Passcode){
            var data = JSON.stringify({PhoneNumber: this.PhoneNumber,Password: this.Password});
            console.log(data);
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/json' );
            let options = new RequestOptions({ headers: headers });
             this.http.post(DOMAIN_ADDRESS+'CreateUserApp', data, options)
             .subscribe(data => {
              
              if(data.json().status=='Success'){
                this.navCtrl.setRoot(ProfilePage, {UserID: data.json().UserID});
              }
              else{
                  let alert = this.alertCtrl.create({
                    title: 'Oops!!',
                    subTitle: 'Try again please..',
                    buttons: ['OK']
                  });
                  alert.present();
              }
             }, error => {
                 console.log("Oops!");
                 let alert = this.alertCtrl.create({
                   title: 'Oops!!',
                   subTitle: 'Something went wrong. Try after sometime',
                   buttons: [
                    {
                      text: 'OK',
                      handler: (data: any) => {
                        console.log('Yes clicked');
                        this.navCtrl.push(SignupPage);
                      }
                    }
                ]
                 });
                 alert.present();
             });
          } else {
            let alert = this.alertCtrl.create({
              title: 'Oops!!',
              subTitle: 'OTP not matched',
              buttons: ['OK']
            });
            alert.present();
            return false;
          }
      } else if (this.PageFrom == 'ForgotPassword'){
        if (otp == this.Passcode){
          this.navCtrl.push(ResetPasswordPage, {PhoneNumber: this.PhoneNumber});
        } else {
          let alert = this.alertCtrl.create({
            title: 'Oops!!',
            subTitle: 'OTP not matched',
            buttons: ['OK']
          });
          alert.present();
          return false;
        }
      }
    }
    
  }
}
