import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { DOMAIN_ADDRESS } from '../../app/const';
import { API_URL } from '../../app/const';
import { orderPage } from '../../pages/order/order';

@Component({
  selector: 'page-productView',
  templateUrl: 'productView.html'
})
export class ProductViewPage {
  product: any;
  productImages: Array<{}>;
  productInfo: Array<{ infoID: string, size: string, quantity: string }>;
  selectedSize: string;
  selectedRow: Number;
  showRemoveFromCart: boolean = false;
  showAddToCart: boolean = true;
  constructor(public navCtrl: NavController,  private http: Http, public alertCtrl: AlertController, public navParams: NavParams) {
    this.product = navParams.get('product');
    var productID = this.product.productID;
    var img1 = this.product.img.toString();
    var productImage = [];
    img1 = img1.substr(img1.length - 4);

    var img2 = this.product.img1.toString();
    img2 = img2.substr(img2.length - 4);

    var img3 = this.product.img2.toString();
    img3 = img3.substr(img3.length - 4);

    var img4 = this.product.img3.toString();
    img4 = img4.substr(img4.length - 4);

    console.log(img1+" , "+img2+" , "+img3+" , "+img4);
    
    if(img1 === '.png') {
      productImage.push(this.product.img);
    }
    if(img2 === '.png') {
      productImage.push(this.product.img1);
    }
    if(img3 === '.png') {
      productImage.push(this.product.img2);
    }
    if(img4 === '.png') {
      productImage.push(this.product.img3);
    }

    let productArray = [];
    let cartListJSON = localStorage.getItem('myCartList');
    if(cartListJSON === null || cartListJSON === '' || cartListJSON === '[]' || cartListJSON === undefined) {
      
    } else {
      productArray = JSON.parse(cartListJSON);
      for(let i = 0; i < productArray.length; i++) {
        if(productArray[i]['productID'] == this.product.productID) {
          this.showAddToCart = false;
          this.showRemoveFromCart = true;
        }
      }
    }


    this.productImages = productImage;
    this.http.get(DOMAIN_ADDRESS + "GetProductInfoByProductID/" + productID)
        .subscribe(data => {
            if(data.json().status == 'success') {
              if(data.json().record_count === 0) {
                console.log(data.json());
              } else {
                var info = data.json().records;
                // console.log(info);
                this.productInfo=[];
                for(let i = 0; i < info.length; i++) {
                  this.productInfo.push({
                     infoID: info[i]['addInfoID'], 
                     size: info[i]['addInfoValue'], 
                     quantity: info[i]['qty']
                  });
                }
              } 
            } else {
              const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
            }
        }, error => {
            const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
            });
            alert.present();
        });
  }
  addToCart() {
    if(this.selectedSize === '' || this.selectedSize === undefined) {
      if(this.showAddToCart) {
        const alert = this.alertCtrl.create({
            title: 'Oops!!',
            subTitle: 'Please Select a Size',
            buttons: ['OK']
        });
        alert.present();
      } else {
        this.showAddToCart = true;
        this.showRemoveFromCart = false;
        let productArray = [];
        let cartListJSON = localStorage.getItem('myCartList');
        productArray = JSON.parse(cartListJSON);
        for(let i = 0; i < productArray.length; i++) {
          if(productArray[i]['productID'] == this.product.productID) {
            productArray.splice(i, 1);
          }
        }
        localStorage.setItem('myCartList', JSON.stringify(productArray));
      }
    } else {
      if(this.showAddToCart) {
        this.showAddToCart = false;
        this.showRemoveFromCart = true;
        let productArray = [];
        let cartListJSON = localStorage.getItem('myCartList');
        if(cartListJSON === null || cartListJSON === '' || cartListJSON === '[]' || cartListJSON === undefined) {
          productArray.push(this.product);
          localStorage.setItem('myCartList', JSON.stringify(productArray));
        } else {
          productArray = JSON.parse(cartListJSON);
          productArray.push(this.product);
          localStorage.setItem('myCartList', JSON.stringify(productArray));
        }
      } else {
        this.showAddToCart = true;
        this.showRemoveFromCart = false;
        let productArray = [];
        let cartListJSON = localStorage.getItem('myCartList');
        productArray = JSON.parse(cartListJSON);
        for(let i = 0; i < productArray.length; i++) {
          if(productArray[i]['productID'] == this.product.productID) {
            productArray.splice(i, 1);
          }
        }
        localStorage.setItem('myCartList', JSON.stringify(productArray));
      }
    }
  }
  sizeSelect(size, i) {
    this.selectedSize = size;
    this.selectedRow = i;
    this.product.size = size;
    // console.log(i);
  }
  buyNow() {
    if(this.selectedSize === '' || this.selectedSize === undefined) {
      const alert = this.alertCtrl.create({
          title: 'Oops!!',
          subTitle: 'Please Select a Size',
          buttons: ['OK']
      });
      alert.present();
    } else {
      this.navCtrl.push(orderPage, {
        size: this.selectedSize,
        product: this.product,
      });
    }
    console.log(this.selectedSize);
  }
}
