import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ProductPage } from '../pages/product/product';
import { ProductViewPage } from '../pages/productView/productView';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPasswordPage } from '../pages/forgotpassword/forgotpassword';
import { OTPPage } from '../pages/otp/otp';
import { ResetPasswordPage } from '../pages/resetpassword/resetpassword';
import { ProfilePage } from '../pages/profile/profile';
import { orderPage } from '../pages/order/order';
import { ModalContentPage } from '../pages/order/order';
import { cartPage } from '../pages/cart/cart';
import { ProfileViewPage } from '../pages/profileView/profileView';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ProductPage,
    ProductViewPage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    OTPPage,
    ResetPasswordPage,
    ProfilePage,
    orderPage,
    ModalContentPage,
    cartPage,
    ProfileViewPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
        scrollPadding: false,
        scrollAssist: true,
        autoFocusAssist: false
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ProductPage,
    ProductViewPage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    OTPPage,
    ResetPasswordPage,
    ProfilePage,
    orderPage,
    ModalContentPage,
    cartPage,
    ProfileViewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
