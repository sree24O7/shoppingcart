import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ProductPage } from '../pages/product/product';
import { ProductViewPage } from '../pages/productView/productView';
import { LoginPage } from '../pages/login/login';
import { ResetPasswordPage } from '../pages/resetpassword/resetpassword';
import { ProfilePage } from '../pages/profile/profile';
import { OTPPage } from '../pages/otp/otp';
import { cartPage } from '../pages/cart/cart';
import { ProfileViewPage } from '../pages/profileView/profileView';


import { Http } from '@angular/http';
import { AlertController,ViewController,LoadingController } from 'ionic-angular';
import { DOMAIN_ADDRESS, API_URL } from './const';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon: string}>;
  // categories: Array<{}>;
  categoryList: Array<{catID: string, Name: string, source: string}>;
  menuIcons: Array<{Name: string, source: string}>;
  
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private http: Http, public alertCtrl: AlertController,) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.http.get(DOMAIN_ADDRESS + "GetAllCategotry/")
        .subscribe(data => {
            if(data.json().status == 'success') {
              if(data.json().record_count === 0) {
                
              } else {
                  // console.log(data.json().records);
                  var categories = data.json().records;
                  var catList = [];
                  // for(let i = 0; i < categories.length; i++) {
                  //   catList.push({
                  //     catID: categories[i]['CategoryID'],
                  //      Name: categories[i]['CategoryName'],
                  //      source: `${API_URL}${categories[i]['icon']}`
                  //   });
                  // }
                  // this.categoryList = catList;
                  this.categoryList=[
                    {catID: categories[0]['CategoryID'], Name: categories[0]['CategoryName'], source: 'assets/images/menuIcons/male.png'},
                    {catID: categories[1]['CategoryID'], Name: categories[1]['CategoryName'], source: 'assets/images/menuIcons/femaleIcon.png'},
                    {catID: categories[2]['CategoryID'], Name: categories[2]['CategoryName'], source: 'assets/images/menuIcons/kidsIcon.png'}
                  ];
                  this.menuIcons=[
                    {Name: 'myOrders', source: 'assets/images/menuIcons/ordersIcon.png'},
                    {Name: 'myCart', source: 'assets/images/menuIcons/cartIcon.png'},
                    {Name: 'myAccount', source: 'assets/images/menuIcons/myAccount.png'}
                  ];
              }
            } else {
              const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
              });
              alert.present();
            }
        }, error => {
            const alert = this.alertCtrl.create({
                title: 'Oops!!',
                subTitle: 'Something went wrong. Try after sometime',
                buttons: ['OK']
            });
            alert.present();
        });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // this.nav.setRoot(ListPage);
    this.nav.push(ListPage, {
      categoryID: page.catID,
      categoryName: page.Name
    })
  }
  homePage() {
    this.nav.setRoot(HomePage);
  }

  MyAccount(){
    var localShoppingData = localStorage.getItem("ShoppingCartDetails");
    console.log(localShoppingData);
    if(localShoppingData==null || localShoppingData=="" || localShoppingData == undefined){
      this.nav.setRoot(LoginPage);
    } else if(localShoppingData == 'undefined'){
      this.nav.setRoot(LoginPage);
    } else{
      this.nav.push(ProfileViewPage);
    }
    
  }
  MyCart() {
    this.nav.push(cartPage);
  }
}
